// RainJson.cpp : 定义 DLL 应用程序的导出函数。
//

#include "stdafx.h"
#include <map>

#include <regex>
#include <RainmeterAPI.h>
#include "cJSON\cJSON.h"
#include "Plugin.h"

PLUGIN_EXPORT void Initialize(void** data, void* rm) {
	Json* measure = new Json;
	*data = measure;
}

PLUGIN_EXPORT void Reload(void* data, void* rm, double* maxValue) {
	Json* measure = (Json*)data;
	auto jsonString = RmReadString(rm, L"JsonString", L"");
	if (wcscmp(jsonString, L"") == 0) {
		RmLog(LOG_ERROR, L"RainJson.dll: invalid \"JsonString\" option.");
	} else if (measure->originJson.compare(jsonString) != 0) { // 只有在Json文本发生改变的情况下才重新解析Json文本
		RmLogF(rm, LOG_DEBUG, L"RainJson.dll: JsonString -> %s", jsonString);
		measure->cJson = std::shared_ptr<JsonWrappr>(new JsonWrappr(cJSON_Parse(wideToNarrow(jsonString).get())));
		measure->originJson = jsonString; // 保存解析过的Json文本
		if (measure->cJson.get()->get() == nullptr) {
			RmLog(LOG_ERROR, L"RainJson.dll: Json文本格式错误");
		}
	}
	auto queryString = RmReadString(rm, L"QueryString", L"");
	if (wcscmp(queryString, L"") == 0) {
		RmLog(LOG_ERROR, L"RainJson.dll: invalid \"QueryString\" option.");
	} else if (measure->originQueryString.compare(queryString) != 0) { // 只有在查询命令发生改变的情况下才会重新解析查询命令
		RmLogF(rm, LOG_DEBUG, L"RainJson.dll: QueryString -> %s", queryString);
		measure->originQueryString = queryString;
		measure->argItemList = parse(queryString);
	}
}

//PLUGIN_EXPORT double Update(void* data)
//{
//	Measure* measure = (Measure*)data;
//	return 0.0;
//}

PLUGIN_EXPORT LPCWSTR GetString(void* data) {
	Json* measure = (Json*)data;

	if (measure->cJson == nullptr || measure->cJson.get()->get() == nullptr) {
		RmLog(LOG_DEBUG, L"RainJson.dll: json格式错误或JsonString的值为空");
		return L"";
	}
	auto fileJson = measure->cJson.get()->get();

	auto argList = measure->argItemList;
	cJSON *pTempJson = fileJson;
	size_t currIndex = 0;
	try {
		for (size_t i = 0; i < argList.size(); ++i) {
			auto arg = argList[i];
			currIndex = i;
			if (arg.type == ArgType::ARRAY) {
				pTempJson = cJSON_GetArrayItem(pTempJson, std::stoi(arg.value));
			} else if (arg.type == ArgType::OBJECT) {
				pTempJson = cJSON_GetObjectItem(pTempJson, arg.value.c_str());
			}

			if (pTempJson == NULL) {
				// 没有根据命令找到指定的Json对象值
				auto wideArg = narrowToWide(arg.origin.c_str());
				LPCWSTR pWideArg = wideArg.get();
				std::wstring errStr;
				errStr.append(L"RainJson.dll: 未找到");
				errStr.append(wideArg.get());
				errStr.append(L"项");
				RmLog(LOG_ERROR, errStr.c_str());
				return L"";
			}
		}
		auto chJson = cJSON_Print(pTempJson);
		auto wJsonPtr = narrowToWide(chJson);
		free(chJson);
		chJson = NULL;
		size_t len = wcslen(wJsonPtr.get());
		if (len >= 2 && wJsonPtr.get()[0] == L'\"' && wJsonPtr.get()[len - 1] == L'\"') {
			measure->result = std::wstring(wJsonPtr.get(), 1, len - 2);
		} else {
			measure->result = std::wstring(wJsonPtr.get());
		}
		return measure->result.c_str();
	} catch (const std::invalid_argument&) {
		std::string errStr("RainJson.dll: 查询语句格式错误(只能包含数字): " + argList[currIndex].origin);
		RmLog(LOG_ERROR, narrowToWide(errStr.c_str()).get());
		return L"";
	} catch (const std::exception& e) {
		std::string errStr("RainJson.dll: ");
		errStr.append(e.what());
		RmLog(LOG_ERROR, narrowToWide(errStr.c_str()).get());
		return L"";
	}
	return L"";
}

//PLUGIN_EXPORT void ExecuteBang(void* data, LPCWSTR args) {
//	Measure* measure = (Measure*)data;
//}

PLUGIN_EXPORT void Finalize(void* data) {
	Json* measure = (Json*)data;
	delete measure;
}

std::vector<ArgItem> parse(const std::wstring & queryString) {
	std::vector<ArgItem> argList;
	std::string paramStr(wideToNarrow(queryString.c_str()).get());
	for (std::regex_iterator<std::string::iterator> iter(paramStr.begin(), paramStr.end(), paramRegex), end;
		iter != end;
		++iter) {
		ArgItem item;
		auto origin = iter->str();
		item.origin = origin;
		item.value = origin.substr(1, origin.length() - 2);
		item.type = origin[0] == '[' ? ArgType::ARRAY : ArgType::OBJECT;
		argList.push_back(item);
	}
	return argList;
}

std::shared_ptr<wchar_t> narrowToWide(const char * const pNarrow) {
	int len = MultiByteToWideChar(CP_ACP, 0, pNarrow, -1, NULL, 0);
	std::shared_ptr<wchar_t> result(new wchar_t[len]);
	MultiByteToWideChar(CP_ACP, 0, pNarrow, -1, result.get(), len);
	return result;
}

std::shared_ptr<char> wideToNarrow(const wchar_t * const pWide) {
	int len = WideCharToMultiByte(CP_OEMCP, NULL, pWide, -1, NULL, 0, NULL, FALSE);
	std::shared_ptr<char> result(new char[len]);
	WideCharToMultiByte(CP_OEMCP, NULL, pWide, -1, result.get(), len, NULL, FALSE);
	return result;
}