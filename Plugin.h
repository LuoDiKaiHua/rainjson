#pragma once

enum ArgType {
	OBJECT,
	ARRAY
};

struct ArgItem {
	std::string origin;
	std::string value;
	ArgType type;
};

class JsonWrappr {
public:
	JsonWrappr(cJSON* jsonPtr) :mJsonPtr(jsonPtr) {}
	JsonWrappr(const JsonWrappr&) = delete;
	~JsonWrappr() {
		if (mJsonPtr != nullptr) {
			cJSON_Delete(mJsonPtr);
			mJsonPtr = nullptr;
		}
	}

	cJSON* get() {
		return mJsonPtr;
	}

private:
	cJSON* mJsonPtr;
};

struct Json {
	std::shared_ptr<JsonWrappr> cJson;
	std::vector<ArgItem> argItemList;
	std::wstring result;
	std::wstring originJson;
	std::wstring originQueryString;
};

static std::regex paramRegex("\\{.*?\\}(?=[\\{\\[])|\\[.*?\\](?=[\\{\\[])|\\{.*?\\}$|\\[.*?\\]$");

std::vector<ArgItem> parse(const std::wstring & queryString);
std::shared_ptr<wchar_t> narrowToWide(const char* const pNarrow);
std::shared_ptr<char> wideToNarrow(const wchar_t* const pWide);